
How to Install Extra Files


You will always need to download and have the base Ravenhearst mod installed. Any additions like Casual or Hell on Earth are JUST the changed files. 

To install an addon, make sure to download and install the default RH Mod you wish to play. Inside Extra Files are the zip files. Select the version you wish to play and extract it. Once extracted you can copy the contents of that folder OVER the default RH mod and start it from that .exe


Extra Files Versions


Disabled Weather - Disables body temperature so you do not have to play with weather

Hell on Earth - Increases zombie spawning, screamer activity, gamestages and wandering hordes for a more action packed edition while using Lesser Cities to give a more isolated feel to the world. 

Casual Version - Awards second support class at the end of the quest line, quicker levelling, stone tools do not break, slower gamestages, quicker grwoing times and more balances to make the RH experience a bit easier.

Denser Cities - Mixer made by Makabriel that adds a large amount of towns and cities in close proximity of each other. People with lighter systems take caution when using this version as it is resource heavy.

Lesser Cities - A more rural scavenger type Mixer made by Makabriel. Good for those who want harder play and less towns and cities with the focus on Nomad and travel.

Default Mixer - Our normal RH default mixer in case you need to switch back